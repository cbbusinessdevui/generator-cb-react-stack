# generator-cb-react-stack

A react/redux generator with some helpers and testing tools built in.

## Tech Stack

* [react](https://facebook.github.io/react/) - View layer
* [redux](https://github.com/reactjs/redux) - State management
* [normalizr](https://github.com/paularmstrong/normalizr) - Normalized JSON
* [sass](http://sass-lang.com/) - CSS preprocessor
* [react-css-modules](https://github.com/gajus/react-css-modules) - Scoped CSS modules
* [babel](https://babeljs.io/) - ES6/JSX compiler
* [webpack](https://webpack.github.io/) - Module bundler
* [Karma](https://karma-runner.github.io/1.0/index.html) - Test suite runner
* [Jasmine](https://jasmine.github.io/) - Behavior-driven development testing framework

## Dev Tooling

* Hot module replacement (using [webpack-hot-middleware](https://github.com/glenjamin/webpack-hot-middleware))
* Redux time travel environment (using [redux-devtools](https://github.com/gaearon/redux-devtools), [redux-devtools-log-monitor](https://github.com/gaearon/redux-devtools-log-monitor))
* Redux action log (using [redux-logger](https://github.com/evgenyrodionov/redux-logger))

## Prerequisite

* [Node.js v4 or higher] (https://nodejs.org/en/download/package-manager/)
* [git] (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Yeoman] (http://yeoman.io/codelab/setup.html)

## Setup

First thing's first, let's install Yeoman globally:
                     
```
$ npm install -g yo
```

Now let's clone the generator to your local machine:

```
$ git clone git@bitbucket.org:cbbusinessdevui/generator-cb-react-stack.git
```

Since we're installing this generator from a local copy (instead of from npm) we need to create a symlink from your ~/.npm/ directory to this generator.. to this, just CD into the cloned generator directory (generator-cb-react-stack/) and type the following:

```
$ sudo npm link
```

That will install your project dependencies and symlink a global module to your local file. After npm is done, you'll be able to call `yo cb-react-stack` and generate a scaffolding in any project directory. 

## Usage

Make a folder for your app:

```
$ mkdir app-name && cd app-name
```

Generate the scaffolding:

```
$ yo cb-react-stack
```

Boot up the app at [http://localhost:3000](http://localhost:3000):

```
$ npm start
```